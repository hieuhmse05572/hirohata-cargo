import 'dart:convert';
import 'dart:math';

class Metal {
  int id;
  Point pos;
  dynamic height;
  dynamic width;
  dynamic length;
  int type;
  dynamic corners;

  Metal(id, pos, height, width, length, type) {
    this.id = id;
    this.pos = pos;
    this.height = height;
    this.width = width;
    this.length = length;
    this.type = type;
  }

  // get the four side coordinates of the rectangle
  double get bottom {
    return pos.y + this.height * 1.0;
  }

  double get left {
    return pos.x * 1.0;
  }

  double get right {
    return pos.x + this.width * 1.0;
  }

  double get top {
    return pos.y * 1.0;
  }

  isCollision(Metal metal) {
    if (this.right < metal.left) {
      print("aaaaaaaaaaaaaaaaaaaaaaaa");
    }
    if (this.top > metal.bottom ||
        this.right < metal.left ||
        // this.bottom < metal.top ||
        this.left > metal.right) {
      return false;
    }

    return true;
  }

  factory Metal.fromJson(Map<String, dynamic> json) => Metal(
      json["id"],
      Point(json["posX"] as double, json["posY"] as double),
      json["height"][0],
      json["width"][0],
      json["length"][0],
      json["type"]);
}
