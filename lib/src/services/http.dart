import 'dart:convert';

import 'package:app/bloc.dart';
import 'package:app/src/model/metal.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:provider/provider.dart';

class HttpService {
  static String postsURL =
      "https://cargo.nsmp-system.com/HirohataCargo/api/viewResult";
  static Map<String, String> requestHeaders = {
    "Content-Type": "application/json",
    "Authorization":
        "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIwMDAwNSIsInJvbGUiOiJLIiwiZXhwIjoxNjAxMjU1ODI5fQ.CyUmneMXITAfX7mw-2zqMqKbLN_QrvjyqvqLOnRa8F4mF7gXJ1mXb6ocJ6vLq-RDOcEihIz1LQna7LyGplE8CA"
  };

  static Future<String> getPosts(BuildContext context) async {
    final dataModel = Provider.of<DataNotifier>(context, listen: true);

    Response res = await post(postsURL, headers: requestHeaders);
    if (res.statusCode == 200) {
      final data = jsonDecode(res.body);
      for (Map i in data) {
        dataModel.metals.add(Metal.fromJson(i));
        dataModel.init();
      }
      return "200";
    } else {
      throw "Can't get posts.";
    }
  }
}
