import 'dart:math';

import 'package:app/bloc.dart';
import 'package:app/configs/colors.dart';
import 'package:app/screen/edit.dart';
import 'package:app/screen/infor.dart';
import 'package:app/screen/list.dart';
import 'package:app/screen/login.dart';
import 'package:app/src/model/metal.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

import 'configs/config.dart';

void main() {
  // runApp(MyApp());

  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]).then((_) {
    runApp(MyApp());
  });
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Hirohata Cargo',
      initialRoute: '/login',
      routes: {
        // When navigating to the "/" route, build the FirstScreen widget.
        '/login': (context) => Login(),
        // When navigating to the "/second" route, build the SecondScreen widget.
        '/listOrder': (context) => OrdersList(),
        '/main': (context) => ChangeNotifierProvider(
            create: (context) => DataNotifier(), child: HomePage()),
        '/edit': (context) => ChangeNotifierProvider(
            create: (context) => DataNotifier(), child: EditScreen()),
      },
    );
  }
}

class DragWidget extends StatefulWidget {
  final Metal metal;
  DragWidget({this.metal});

  @override
  _DragWidget createState() => _DragWidget();
}

class _DragWidget extends State<DragWidget> {
  bool accepted = false;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    Color color = widget.metal.type == 1 ? ColorsOf.border1 : ColorsOf.border2;
    DataNotifier dataNotifier = Provider.of<DataNotifier>(context);

    return Positioned(
      top: dataNotifier.height - widget.metal.pos.x * Config.scale,
      left: (widget.metal.pos.y + 20) * Config.scale,
      child: Draggable<Metal>(
        child: box(widget.metal.height, widget.metal.width, color,
            widget.metal.length[0]),
        feedback: boxFeedback(widget.metal.height, widget.metal.width, color,
            widget.metal.length[0]),
        childWhenDragging: Container(),
        data: widget.metal,
        onDragStarted: () {
          dataNotifier.currentDrag = widget.metal;
          dataNotifier.removeBorder(widget.metal);
        },
        onDragEnd: (dragDetails) {
          if (dataNotifier.bottom > -1) {
            Point newPos = Point(
                dataNotifier.bottom, dragDetails.offset.dx / Config.scale - 20);
            dataNotifier.updateNewPosBottom(newPos);
            print(dragDetails.offset);
          }

          if (dataNotifier.left > -1) {
            Point newPos = Point(
                (dataNotifier.height - dragDetails.offset.dy - 10) /
                    Config.scale,
                dataNotifier.left);
            dataNotifier.updateNewPosLeft(newPos);
          }
        },
      ),
    );
  }
}

// class MoveableStackItem extends StatefulWidget {
//   MoveableStackItem({Key key, this.metal}) : super(key: key);

//   final Metal metal;
//   @override
//   State<StatefulWidget> createState() {
//     return _MoveableStackItemState();
//   }
// }

// class _MoveableStackItemState extends State<MoveableStackItem> {
//   double xPosition = 0;
//   double yPosition = 0;
//   @override
//   void initState() {
//     final dataModel = Provider.of<DataNotifier>(context, listen: true);

//     yPosition = dataModel.height - widget.metal.pos.x * Config.scale;
//     xPosition = (widget.metal.pos.y + 20) * Config.scale;
//     super.initState();
//   }

//   @override
//   Widget build(BuildContext context) {
//     Color color = widget.metal.type == 1 ? ColorsOf.border1 : ColorsOf.border2;
//     final dataModel = Provider.of<DataNotifier>(context, listen: true);

//     return Positioned(
//       top: yPosition,
//       left: xPosition,
//       child: GestureDetector(
//         onTapDown: (details) {
//           dataModel.bringToFront(Key(widget.metal.id));
//         },
//         onPanUpdate: (tapInfo) {
//           setState(() {
//             xPosition += tapInfo.delta.dx * 1.2;
//             yPosition += tapInfo.delta.dy * 1.2;
//           });

//           print(xPosition);
//           print(yPosition);
//         },
//         child: box(widget.metal.height, widget.metal.width, color),
//       ),
//     );
//   }
// }

Widget box(int height, int width, Color border, int length) {
  return Container(
    height: height * Config.scale,
    width: width * Config.scale,
    // margin: const EdgeInsets.all(30.0),
    // padding: const EdgeInsets.all(10.0),
    decoration: myBoxDecoration(border,
        Color.fromRGBO(68, 84, 106, 1)), //             <--- BoxDecoration here
    child: Stack(
      children: [
        Container(
          margin: const EdgeInsets.all(6),
          decoration: myBoxDecoration(
              Colors.grey,
              Color.fromRGBO(
                  68, 84, 106, 1)), //             <--- BoxDecoration here
          child: Center(
            child: Text(
              "$length/8",
              style: TextStyle(fontSize: 20.0, color: Colors.white),
            ),
          ),
        ),
        Positioned(
          top: height * Config.scale / 2 - 20,
          left: 0,
          child: RotatedBox(
            quarterTurns: -1,
            child: Center(
              child: Text(
                "$height/32",
                style: TextStyle(fontSize: 18.0, backgroundColor: Colors.white),
              ),
            ),
          ),
        ),
        Positioned(
          top: height * Config.scale - 20,
          left: width * Config.scale / 2,
          child: Center(
            child: Text(
              "$width/32",
              style: TextStyle(fontSize: 18.0, backgroundColor: Colors.white),
            ),
          ),
        ),
      ],
    ),
  );
}

Widget boxFeedback(int height, int width, Color border, int length) {
  return Container(
    height: height * Config.scale,
    width: width * Config.scale,
    // margin: const EdgeInsets.all(30.0),
    // padding: const EdgeInsets.all(10.0),
    decoration: myBoxDecoration(
        border.withOpacity(0.3),
        Color.fromRGBO(
            68, 84, 106, 0.5)), //             <--- BoxDecoration here
    child: Container(
      margin: const EdgeInsets.all(6),
      decoration: myBoxDecoration(
          Color.fromRGBO(255, 255, 255, 0.5),
          Color.fromRGBO(
              68, 84, 106, 1)), //             <--- BoxDecoration here
      child: Center(
        child: Text(
          "$length/x8",
          style: TextStyle(fontSize: 20.0, color: Colors.black),
        ),
      ),
    ),
  );
}

Widget wid(double top, double left, double height, double width) {
  return Positioned(
    top: 500 - top * Config.scale,
    left: (left + 20) * Config.scale,
    child: Container(
      decoration: boxWithImage(),
      height: height * Config.scale,
      width: width * Config.scale,
      // color: Colors.brown,
      // margin: const EdgeInsets.all(30.0),
      // padding: const EdgeInsets.all(10.0),
    ),
  );
}

BoxDecoration myBoxDecoration(Color bg, Color border) {
  return BoxDecoration(
    color: bg,
    border: Border.all(
      color: border, //                   <--- border color
      width: 1,
    ),
  );
}

BoxDecoration boxWithImage() {
  return BoxDecoration(
    image: new DecorationImage(
      image: new AssetImage("assets/images/bg.jpg"),
      fit: BoxFit.fitWidth,
    ),
  );
}
