import 'package:app/src/model/metal.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'bloc.dart';
import 'configs/config.dart';
import 'main.dart';

List<Widget> createWidget(Metal metal) {
  List<Widget> list = [];

  list.add(DragWidget(metal: metal));
  list.add(Top(
    metal: metal,
  ));
  list.add(Right(
    metal: metal,
  ));
  return list;
}

class Top extends StatefulWidget {
  final Metal metal;
  Top({this.metal});

  @override
  _Top createState() => _Top();
}

class _Top extends State<Top> {
  bool accepted = false;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    DataNotifier dataNotifier = Provider.of<DataNotifier>(context);

    Widget initWidget = Container(
      height: 60,
      width: widget.metal.width * Config.scale,
      color: Colors.transparent,
    );
    Widget hoverWidget = Container(
      height: 60,
      width: widget.metal.width * Config.scale,
      color: Colors.green,
    );
    return Positioned(
      top: dataNotifier.height - widget.metal.pos.x * Config.scale - 40,
      left: (widget.metal.pos.y + 20) * Config.scale,
      child: Container(
        child: DragTarget(
          builder: (context, List<Metal> candidateData, rejectedData) {
            return accepted ? hoverWidget : initWidget;
          },
          onWillAccept: (data) {
            print(data);
            setState(() {
              accepted = true;
            });
            return true;
          },
          onAcceptWithDetails: (details) {},
          onAccept: (data) {
            debugPrint('draggable is on the target TOp TOPPPP');
            dataNotifier.successDrop = true;
            accepted = false;
            dataNotifier.bottom = widget.metal.pos.x * 1.0;
            // Provider.of<DataNotifier>(context, listen: true)
            //     .updatePosition(data, widget.metal);
          },
          onLeave: (data) {
            dataNotifier.successDrop = false;
            setState(() {
              // color = Colors.green;
              accepted = false;
              // w = draggable(Point(200, 100));
            });
          },
        ),
      ),
    );
  }
}

class Base extends StatefulWidget {
  final Metal metal;
  Base({this.metal});

  @override
  _Base createState() => _Base();
}

class _Base extends State<Base> {
  bool accepted = false;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    DataNotifier dataNotifier = Provider.of<DataNotifier>(context);

    Widget initWidget = Container(
      height: 20,
      width: 700,
      color: Colors.brown,
    );
    Widget hoverWidget = Container(
      height: 30,
      width: 700,
      color: Colors.red,
    );
    return Positioned(
      top: 500 + 80.0,
      left: 20,
      child: Container(
        child: DragTarget(
          builder: (context, List<Metal> candidateData, rejectedData) {
            return accepted ? hoverWidget : initWidget;
          },
          onWillAccept: (data) {
            print(data);
            setState(() {
              accepted = true;
            });
            return true;
          },
          onAcceptWithDetails: (details) {},
          onAccept: (data) {
            debugPrint('draggable is on the target TOp TOPPPP');
            dataNotifier.successDrop = true;
            accepted = false;
            dataNotifier.bottom = 0 * 1.0;
            // Provider.of<DataNotifier>(context, listen: true)
            //     .updatePosition(data, widget.metal);
          },
          onLeave: (data) {
            dataNotifier.successDrop = false;
            setState(() {
              // color = Colors.green;
              accepted = false;
              // w = draggable(Point(200, 100));
            });
          },
        ),
      ),
    );
  }
}

class Right extends StatefulWidget {
  final Metal metal;
  Right({this.metal});

  @override
  _Right createState() => _Right();
}

class _Right extends State<Right> {
  bool accepted = false;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    DataNotifier dataNotifier = Provider.of<DataNotifier>(context);

    Widget initWidget = Container(
      height: widget.metal.height * Config.scale,
      width: 40.0,
      color: Colors.transparent,
    );
    Widget hoverWidget = Container(
        height: widget.metal.height * Config.scale,
        width: 40.0,
        color: Colors.green);

    return Positioned(
      top: dataNotifier.height - widget.metal.pos.x * Config.scale,
      left: (widget.metal.pos.y + 20 + widget.metal.width) * Config.scale - 30,
      child: Container(
        child: DragTarget(
          builder: (context, List<Metal> candidateData, rejectedData) {
            return accepted ? hoverWidget : initWidget;
          },
          onWillAccept: (data) {
            print(data);
            setState(() {
              accepted = true;
            });
            return true;
          },
          onAcceptWithDetails: (details) {},
          onAccept: (data) {
            debugPrint('draggable is on the target right RIGHT------');
            dataNotifier.left = widget.metal.pos.y * 1.0 + widget.metal.width;
            accepted = false;

            // Provider.of<DataNotifier>(context, listen: true)
            //     .updatePosition(data, widget.metal);
          },
          onLeave: (data) {
            setState(() {
              // color = Colors.green;
              accepted = false;
              // w = draggable(Point(200, 100));
            });
          },
        ),
      ),
    );
  }
}
