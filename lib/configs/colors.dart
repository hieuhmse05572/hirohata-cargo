import 'package:flutter/material.dart';

class ColorsOf {
  static Color border1 = Color.fromRGBO(91, 155, 213, 1);
  static Color border2 = Color.fromRGBO(197,90,17, 1);
  static Color borderline = Color.fromRGBO(68, 84, 106, 1);
  static Color search = Color(0xffe4eaf0);

}
