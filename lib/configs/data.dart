import 'dart:math';

import 'package:app/src/model/metal.dart';
import 'package:flutter/material.dart';

import '../main.dart';

class Data {
  static List<Metal> metals = [
    Metal(0, Point(60.0, 0), 60, 60, [12], 1),
    Metal(1, Point(37.0, 60), 37, 45, [9], 1),
    Metal(2, Point(37.0, 105), 37, 45, [10], 1),
    Metal(3, Point(30.0, 150), 30, 35, [6], 2),
    Metal(4, Point(67.0, 60), 30, 36, [8], 2),
    Metal(5, Point(67, 60 + 36), 30, 36, [1], 2),
    Metal(6, Point(67, 60 + 36 + 36), 30, 36, [120], 2),
  ];

  static Metal metalNull = Metal(-1, Point(0.0, 0), 0, 0, [120], 1);
}
