import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:provider/provider.dart';

import 'form.dart';

class Login extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Scaffold(
      body: Center(
        child: Container(
          width: size.width / 2.5,
          height: size.height / 1.2,
          decoration: new BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(12)),
            boxShadow: [
              BoxShadow(
                  color: Color(0xffECECEC).withOpacity(0.3),
                  blurRadius: 4,
                  offset: Offset(1, 1))
            ],
//                          color: Colors.,
          ),
          child: ListView(
            children: <Widget>[
              Container(
                height: 120,
                decoration: BoxDecoration(
                    image: DecorationImage(
//                  image: AssetImage("assets/${giftIndex + 1}.jpg"),
                  image: AssetImage("assets/images/logo.png"),
                  fit: BoxFit.fitHeight,
                )),
              ),
              Container(
                margin: EdgeInsets.only(top: 0, bottom: 65),
                alignment: Alignment.topCenter,
                child: Text(
                  'NIPPON STEEL',
                  style: TextStyle(
                    fontSize: 30,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'iCiel',
                  ),
                ),
              ),
              LoginForm(),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  InkWell(
                    child: Container(
                      height: 40,
                      width: 40,
                    ),
                  ),
                  SizedBox(
                    width: 10,
                    height: 1,
                  ),
                  InkWell(
                    child: Container(
                      height: 40,
                      width: 40,
                      decoration: BoxDecoration(),
                    ),
                  ),
                ],
              ),
              SizedBox(
                width: 1,
                height: 15,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Text('Forgot Password?'),
                  InkWell(
                    onTap: () {
                      Navigator.pushNamed(context, '/signUp');
                    },
                    child: Text(
                      'Sign up',
                      style: TextStyle(color: Colors.red),
                    ),
                  )
                ],
              )
//              Positioned(
//                  bottom: 20,
//                  width: size.width,
//                  child: Row(
//                    mainAxisAlignment: MainAxisAlignment.center,
//                    children: <Widget>[
//                      Text('Don\'t have an account yet?'),
//                      InkWell(
//                          child: Text(
//                        ' Sign Up',
//                        style: TextStyle(color: Dono.all),
//                      ))
//                    ],
//                  )),
            ],
          ),
        ),
      ),
    );
  }
}
