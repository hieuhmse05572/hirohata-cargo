import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

// Create a Form widget.
class LoginForm extends StatefulWidget {
  @override
  LoginFormState createState() {
    return LoginFormState();
  }
}

// Create a corresponding State class.
// This class holds data related to the form.
class LoginFormState extends State<LoginForm> {
  // Create a global key that uniquely identifies the Form widget
  // and allows validation of the form.
  final passController = TextEditingController();

  final userController = TextEditingController();

  // Note: This is a GlobalKey<FormState>,
  // not a GlobalKey<MyCustomFormState>.
  final _formKey = GlobalKey<FormState>();

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    userController.dispose();
    passController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // Build a Form widget using the _formKey created above.
    return Form(
      key: _formKey,
      child: SizedBox(
        child: Padding(
          padding: const EdgeInsets.only(right: 50, left: 50),
          child: Column(
//            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Container(
                alignment: Alignment.topLeft,
                child: Text(
                  'Sign in',
                  style: TextStyle(
                    fontSize: 30,
                    fontWeight: FontWeight.normal,
                    fontFamily: 'iCiel',
                  ),
                ),
              ),
              Container(
                  alignment: Alignment.bottomLeft,
                  child: Text(
                    'Hi there! Nice to see you again',
                    style: TextStyle(
                      fontSize: 15,
                      color: Colors.grey,
                      fontWeight: FontWeight.normal,
                      fontFamily: 'iCiel',
                    ),
                  )),
              SizedBox(
                height: 30,
                width: 1,
              ),
              TextFormField(
                obscureText: false,
                controller: userController,
                decoration: const InputDecoration(
                  labelStyle: TextStyle(fontSize: 15, color: Colors.red),
                  icon: Icon(Icons.person_outline),

                  labelText: 'Email: ',
                  contentPadding:
                      EdgeInsets.only(bottom: 3, top: 30), //here your padding
//              fillColor: Colors.white
                ),
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Please enter some text';
                  }
                  return null;
                },
              ),
              TextFormField(
                obscureText: true,
                controller: passController,
                decoration: const InputDecoration(
                  icon: Icon(Icons.lock_outline),
                  labelStyle: TextStyle(fontSize: 15, color: Colors.red),
                  labelText: 'Password: ',
                  contentPadding:
                      EdgeInsets.only(bottom: 3, top: 30), //here your padding
//              fillColor: Colors.white
                ),
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Please enter some text';
                  }
                  return null;
                },
              ),
              Container(
                  alignment: Alignment.topCenter, child: _loginButton(context))
            ],
          ),
        ),
      ),
    );
  }

  Widget _loginButton(BuildContext context
//    RunMutation runMutation,
      ) {
//    final userBloc = Provider.of<UserBloc>(context);
    return Padding(
        padding: const EdgeInsets.symmetric(
          vertical: 30.0,
        ),
        child: InkWell(
          onTap: () {
            Navigator.pushNamed(context, '/listOrder');
          },
          child: Container(
            height: 40,
            width: 220,
            decoration: new BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(12)),
              boxShadow: [
                BoxShadow(
                  color: Colors.red,
                )
              ],
//                          color: Colors.,
            ),
            child: Center(
                child: Text(
              'Sign in ',
              style: TextStyle(
                color: Colors.white,
                fontFamily: 'iCiel',
                fontSize: 16,
              ),
            )),
          ),
        ));
  }
}
