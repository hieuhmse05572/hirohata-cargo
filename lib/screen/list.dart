import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class OrdersList extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new _OrdersListState();
  }
}

List<Order> orders = [
  Order(name: 'Order ID', date: 'LastModified: 12/12/2020'),
  Order(name: 'Order ID', date: 'LastModified: 12/12/2020'),
  Order(name: 'Order ID', date: 'LastModified: 12/12/2020'),
  Order(name: 'Order ID', date: 'LastModified: 12/12/2020'),
  Order(name: 'Order ID', date: 'LastModified: 12/12/2020'),
  Order(name: 'Order ID', date: 'LastModified: 12/12/2020'),
  Order(name: 'Order ID', date: 'LastModified: 12/12/2020'),
];

class _OrdersListState extends State<OrdersList> {
  TextEditingController searchController = new TextEditingController();
  String filter;

  @override
  initState() {
    searchController.addListener(() {
      setState(() {
        filter = searchController.text;
      });
    });
  }

  @override
  void dispose() {
    searchController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            title: Text('List Order',
                style: TextStyle(
                    color: Colors.white, fontWeight: FontWeight.bold))),
        body: new Column(
          children: <Widget>[
            new Padding(
              padding: new EdgeInsets.all(8.0),
              child: new TextField(
                controller: searchController,
                decoration: InputDecoration(
                  hintText: 'Search Order ID',
                  contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(32.0)),
                ),
              ),
            ),
            new Expanded(
              child: new ListView.builder(
                itemCount: orders.length,
                itemBuilder: (context, index) {
                  // if filter is null or empty returns all data
                  return filter == null || filter == ""
                      ? ListTile(
                          title: Text('${orders[index].name}'),
                          subtitle: Text('${orders[index].date}'),
                          trailing: Container(
                            width: 100,
                            child: Row(
                              children: [
                                Text('View Result'),
                                Icon(
                                  Icons.chevron_right,
                                  color: Colors.grey,
                                  size: 24.0,
                                ),
                              ],
                            ),
                          ),
                          onTap: () {
                            Navigator.pushNamed(context, '/main');
                          },
                        )
                      : '${orders[index].name}'
                              .toLowerCase()
                              .contains(filter.toLowerCase())
                          ? ListTile(
                              title: Text(
                                '${orders[index].name}',
                              ),
                              subtitle: Text('${orders[index].date}'),
                              leading: new CircleAvatar(
                                  backgroundColor: Colors.blue,
                                  child: Text(
                                      '${orders[index].name.substring(0, 1)}')),
                              onTap: () => _onTapItem(context, orders[index]),
                            )
                          : new Container();
                },
              ),
            ),
          ],
        ));
  }

  void _onTapItem(BuildContext context, Order post) {
    Scaffold.of(context).showSnackBar(
        new SnackBar(content: new Text("Tap on " + ' - ' + post.name)));
  }
}

class Order {
  final String name;
  final String date;

  const Order({this.name, this.date});
}
