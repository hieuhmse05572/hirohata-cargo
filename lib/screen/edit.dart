import 'dart:ui';

import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../bloc.dart';

class EditScreen extends StatefulWidget {
  @override
  _EditScreenState createState() => _EditScreenState();
}

class _EditScreenState extends State<EditScreen> {
  @override
  Widget build(BuildContext context) {
    final dataModel = Provider.of<DataNotifier>(context, listen: true);
    dataModel.height = 420;
    // dataModel.items.addAll(createWidget(Data.metals[0]));
    return Scaffold(
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(0.0),
          child: AppBar(
            automaticallyImplyLeading: false, // hides leading widget
            flexibleSpace: Container(),
          )),
      body: Column(
        children: [
          Expanded(
              flex: 2,
              child: Container(
                  decoration: BoxDecoration(
                image: new DecorationImage(
                  image: new AssetImage("assets/images/image.png"),
                  fit: BoxFit.cover,
                ),
              ))),
          Expanded(
            flex: 4,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Expanded(
                  child: DrawWidget(),
                  flex: 4,
                ),
                Expanded(
                    flex: 6,
                    child: DottedBorder(
                      color: Colors.grey,
                      strokeWidth: 2,
                      child: Container(
                          // margin: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
                          decoration: BoxDecoration(
                              // borderRadius: (BorderRadius.circular(3)),
                              // border: Border.all(
                              //     color: Colors.grey, style: BorderStyle.solid),
                              color: Color(0xffFFFAF8)),
                          // height: 550,
                          child: Stack(children: dataModel.items)),
                    )),
                Expanded(
                    flex: 3,
                    child: Container(
                      color: Color(0xffFFFAF8),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Column(
                            children: [
                              CustomCheckBox(),
                              CustomInput(),
                            ],
                          ),
                          Container(
                            margin: EdgeInsets.only(right: 30),
                            alignment: Alignment.bottomRight,
                            child: RaisedButton(
                              color: Colors.blue,
                              onPressed: () {},
                              child: Text("印刷 ",
                                  style: TextStyle(color: Colors.white)),
                            ),
                          )
                        ],
                      ),
                    ))
              ],
            ),
          ),
        ],
      ),
    );
    // body: Stack(children: [
    //   MoveableStackItem(
    //     metal: Data.metals[0],
    //   ),
    //   MoveableStackItem(
    //     metal: Data.metals[1],
    //   )
    // ]));
  }
}

class CustomCheckBox extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<CustomCheckBox> {
  bool topVal = false;
  bool botVal = false;
  @override
  Widget build(BuildContext context) {
    final dataModel = Provider.of<DataNotifier>(context, listen: true);

    return Column(
      children: [
        Container(
          height: 20,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Checkbox(
                onChanged: (bool value) {
                  setState(() {
                    topVal = value;
                  });
                  if (value)
                    dataModel.updateCheckBox(1, "赤・黒");
                  else
                    dataModel.updateCheckBox(1, "");
                },
                value: topVal,
              ),
              Text("ｽﾀﾝｼｮﾝ "),
            ],
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Checkbox(
              onChanged: (bool value) {
                setState(() {
                  botVal = value;
                });
                if (value)
                  dataModel.updateCheckBox(2, "スタンション");
                else
                  dataModel.updateCheckBox(2, "");
              },
              value: botVal,
            ),
            Text("ﾊｯｶｰ卸 "),
          ],
        ),
      ],
    );
  }
}

class CustomInput extends StatefulWidget {
  @override
  _CustomInputState createState() => _CustomInputState();
}

class _CustomInputState extends State<CustomInput> {
  bool topVal = false;
  bool botVal = false;
  @override
  Widget build(BuildContext context) {
    final dataModel = Provider.of<DataNotifier>(context, listen: true);

    return Container(
      margin: EdgeInsets.only(right: 30),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Text("固縛 "),
              Container(
                width: 70,
                height: 30,
                child: TextField(
                  onChanged: (text) {
                    dataModel.updateInput(1, "$text");
                  },
                  decoration: new InputDecoration(
                    focusedBorder: OutlineInputBorder(
                      borderSide:
                          BorderSide(color: Colors.greenAccent, width: 1.0),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey, width: 1.0),
                    ),
                  ),
                ),
              ),
            ],
          ),
          SizedBox(
            height: 3,
            width: 1,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Text("固縛 "),
              Container(
                width: 70,
                height: 30,
                child: TextField(
                  onChanged: (text) {
                    dataModel.updateInput(2, "$text");
                  },
                  decoration: new InputDecoration(
                    focusedBorder: OutlineInputBorder(
                      borderSide:
                          BorderSide(color: Colors.greenAccent, width: 1.0),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey, width: 1.0),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

class DrawWidget extends StatefulWidget {
  @override
  _DrawWidgetState createState() => new _DrawWidgetState();
}

class _DrawWidgetState extends State<DrawWidget> {
  List<Offset> _points = <Offset>[];

  @override
  Widget build(BuildContext context) {
    final dataModel = Provider.of<DataNotifier>(context, listen: true);

    return Stack(
      children: [
        DottedBorder(
                      color: Colors.grey,
                      strokeWidth: 2,
          child:Container(
          color: Color(0xffFFFAF8),
          child: new GestureDetector(
            onPanUpdate: (DragUpdateDetails details) {
              setState(() {
                RenderBox object = context.findRenderObject();
                Offset _localPosition =
                    object.globalToLocal(details.globalPosition);
                _points = new List.from(_points)..add(_localPosition);
              });
            },
            onPanEnd: (DragEndDetails details) => _points.add(null),
            child: new CustomPaint(
              painter: new Signature(points: _points),
              size: Size.infinite,
            ),
          ),
        ),
        ),
        Positioned(
            bottom: 40,
            left: 40,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                dataModel.infor.checkbox1 != ""
                    ? Text(dataModel.infor.checkbox1,
                        style: TextStyle(fontSize: 16))
                    : Container(),
                dataModel.infor.checkbox2 != ""
                    ? Text(dataModel.infor.checkbox2,
                        style: TextStyle(fontSize: 16))
                    : Container(),
                dataModel.infor.text1 != ""
                    ? Text("マンボ ${dataModel.infor.text1}",
                        style: TextStyle(fontSize: 16))
                    : Container(),
                dataModel.infor.text2 != ""
                    ? Text("固縛 ${dataModel.infor.text2}",
                        style: TextStyle(fontSize: 16))
                    : Container(),
              ],
            )),
      ],
    );
  }
}

class Signature extends CustomPainter {
  List<Offset> points;

  Signature({this.points});

  @override
  Future<void> paint(Canvas canvas, Size size) async {
    Paint paint = new Paint()
      ..color = Colors.black
      ..strokeCap = StrokeCap.round
      ..strokeWidth = 2.0;

    for (int i = 0; i < points.length - 1; i++) {
      if (points[i] != null && points[i + 1] != null) {
        canvas.drawLine(points[i], points[i + 1], paint);
      }
    }
    // final recorder = new PictureRecorder();
    // final picture = recorder.endRecording();
    // final img = await picture.toImage(200, 200);
    // final pngBytes = await img.toByteData(format: ImageByteFormat.png);
  }

  @override
  bool shouldRepaint(Signature oldDelegate) => oldDelegate.points != points;
}
