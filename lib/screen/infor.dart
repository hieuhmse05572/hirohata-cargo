import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../bloc.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    // dataModel.items.addAll(createWidget(Data.metals[0]));
    return Scaffold(
        appBar: AppBar(
          // Here we take the value from the MyHomePage object that was created by
          // the App.build method, and use it to set our appbar title.
          title: Text("Order Information"),
        ),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                RaisedButton(
                  color: Colors.blue,
                  onPressed: () {
                    // HttpService.getPosts(context);
                  },
                  child: Text(
                    "OrderID: 12",
                    style: TextStyle(fontSize: 19, color: Colors.white),
                  ),
                ),
              ],
            ),
            Expanded(
              flex: 2,
              child: Container(
                width: 499,
                child: TextFormField(
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    hintText: "Setting thuật toán ở đây",
                    hintStyle: TextStyle(fontSize: 15, color: Colors.grey),
                    contentPadding: EdgeInsets.all(15), //here your padding
//              fillColor: Colors.white
                  ),
                  keyboardType: TextInputType.multiline,
                  maxLines: 7,
                  maxLength: 400,
                ),
              ),
            ),
            Expanded(
              flex: 4,
              child: Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    cannotEdit(context),
                    Container(
                      child: Text("Mặt nhìn từ trên xuống"),
                      alignment: Alignment.center,
                      decoration: new BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(12)),
                          border: Border.all(color: Colors.black)
//                          color: Colors.,
                          ),
                      width: 490,
                      height: 350,
                    )
                  ],
                ),
              ),
            )
          ],
        ));
  }
}

Widget cannotEdit(BuildContext context) {
  final dataModel = Provider.of<DataNotifier>(context, listen: true);
  Size size = MediaQuery.of(context).size;

  return Container(
    height: 350,
    width: (size.width - 100) / 2,
    child: Stack(
      children: [
        Container(
            height: 350,
            width: (size.width - 100) / 2,
            child: Stack(children: dataModel.items)),
        InkWell(
          onTap: () {
            Navigator.pushNamed(context, '/edit');
          },
          child: Container(
            alignment: Alignment.topCenter,
            child: Text(
              "Click to Edit",
              style: TextStyle(fontSize: 20, color: Colors.black),
            ),
            color: Colors.white.withOpacity(0.2),
            height: 350,
            width: double.infinity,
          ),
        ),
      ],
    ),
  );
}
