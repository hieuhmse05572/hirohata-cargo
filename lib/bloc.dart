import 'dart:math';

import 'package:app/main.dart';
import 'package:app/src/model/infor.dart';
import 'package:app/src/model/metal.dart';
import 'package:app/src/model/metal.dart';
import 'package:app/src/model/metal.dart';
import 'package:app/src/model/metal.dart';
import 'package:app/util.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rulers/rulers.dart';

import 'configs/data.dart';

class DataNotifier with ChangeNotifier {
  bool successDrop;
  List<Widget> items = [];
  List<Metal> metals = [];
  Metal currentDrag;
  double bottom = -1.0;
  double left = -1.0;
  double height = 300;
  Infor infor;
  // CardItem acceptedData;
  init() {
    updateWidget();
    notifyListeners();
  }

  removeBorder(Metal metal) {
    items = [];
    metals.forEach((element) {
      if (element.id != metal.id)
        items.addAll(createWidget(element));
      else
        items.add(DragWidget(metal: metal));
    });
  }

  DataNotifier() {
    successDrop = false;
    metals.addAll(Data.metals);
    updateWidget();
    currentDrag = Data.metalNull;
    infor = Infor("", "", "", "");
  }

  updateCheckBox(int checkbox, String content) {
    if (checkbox == 1)
      infor.checkbox1 = content;
    else
      infor.checkbox2 = content;
    notifyListeners();
  }

  updateInput(int text, String content) {
    if (text == 1)
      infor.text1 = content;
    else
      infor.text2 = content;
    notifyListeners();
  }

  set setSuccessDrop(bool status) {
    successDrop = status;
    notifyListeners();
  }

  set setCurrentDrag(Metal metal) {
    currentDrag = metal;
  }

  updateWidget() {
    items = [];
    metals.forEach((element) {
      items.addAll(createWidget(element));
    });
  }

  backToTop(Metal metal) {
    metals.removeWhere((m) => m.id == metal.id);
    metals.add(metal);
    updateWidget();
    // notifyListeners();
  }

  bringToFront(Key key) {
    for (var i = 0; i < items.length; i++) {
      Widget widget = items[i];
      if (key == widget.key) {
        items.remove(widget);
        items.add(widget);
        break;
      }
    }
    notifyListeners();
  }

  // updateWidget() {
  //   items = [];
  //   metals.forEach((element) {
  //     print(element.id);
  //     items.add(MoveableStackItem(key: Key(element.id), metal: element));
  //   });
  // }

  // double get height {
  // return this.height;
  // return currentDrag.id != "null" ? currentDrag.height * 1.0 : 50.0;
  // }

  update() {}

  // changeAcceptedData(CardItem data) {
  //   acceptedData = data;
  //   notifyListeners();
  // }

  // changeSuccessDrop(bool status) {
  //   setSuccessDrop = status;
  // }

  removeLastItem() {
    print("xoa xoa cxoa");
    // metals = [];
    metals.removeAt(0);
    // items = [];
    // metals[0].height = metals[0].height - 20;
    updateWidget();
    notifyListeners();
  }

  updateNewPosBottom(Point point) {
    int index = metals.indexWhere((m) => m.id == currentDrag.id);
    print(currentDrag.pos);
    double temp = point.y;
    double topRight = -1, botRight = -1;
    double topLeft = -1, botLeft = -1;

    for (var i = 0; i < metals.length; i++) {
      Metal element = metals[i];

      if (element.pos.x > point.x &&
          element.pos.y < point.y + currentDrag.width &&
          point.x > element.pos.x - element.height &&
          point.y + currentDrag.width < element.pos.y + element.width) {
        print("cham ben phai duoi");
        botRight = 1.0 * element.pos.y - currentDrag.width;
        // break;
      }
      if (element.pos.x > point.x + currentDrag.height &&
          point.y + currentDrag.width > element.pos.y &&
          point.x + currentDrag.height > element.pos.x - element.height &&
          point.y + currentDrag.width < element.pos.y + element.width) {
        print("cham ben phai tren");
        topRight = 1.0 * element.pos.y - currentDrag.width;
        // break;
      }
      if (element.pos.x > point.x &&
          element.pos.y < point.y &&
          point.x < element.pos.x + element.height &&
          point.y < element.pos.y + element.width) {
        print("ccham ben trai duoi");
        // bot = 1.0 * element.pos.y - currentDrag.width;
        botLeft = 1.0 * element.pos.y + element.width;
        // break;
      }
    }
    if (topRight > 1) temp = topRight;
    if (botLeft > 1) temp = botLeft;
    if (topLeft > 1) temp = topLeft;

    currentDrag.pos = Point(point.x * 1.0 + currentDrag.height, temp * 1.0);
    bottom = -1.0;
    metals[index] = currentDrag;
    updateWidget();
    notifyListeners();
  }

  doCollide(x1, y1, w1, x2, y2, w2) {
    var xd = x1 - x2;
    var yd = y1 - y2;
    var wt = w2 + w1;
    return (xd * xd + yd * yd <= wt * wt);
  }

  isCollide(a, b) {
    return !(((a.y + a.height) < (b.y)) ||
        (a.y > (b.y + b.height)) ||
        ((a.x + a.width) < b.x) ||
        (a.x > (b.x + b.width)));
  }

  updateNewPosLeft(Point point) {
    int index = metals.indexWhere((m) => m.id == currentDrag.id);
    currentDrag.pos = Point(point.x + currentDrag.height, point.y);
    left = -1.0;
    metals[index] = currentDrag;
    updateWidget();
    notifyListeners();
  }

  updatePosition(Metal metalOld, Metal metalNew) {
    // metals.removeAt();
    // items = [];
    int index = metals.indexWhere((m) => m.id == metalOld.id);
    metalOld.pos = Point(metalNew.pos.x + metalOld.height, metalNew.pos.y);
    metals[index] = metalOld;
    // metals.removeAt(index);
    // metals[0].height = metals[0].height - 20;
    // currentDrag = Data.metalNull;
    updateWidget();
    notifyListeners();
  }

  // addItemToList(CardItem item) {
  //   items.add(item);
  //   notifyListeners();
  // }

  initializeDraggableList() {
    updateWidget();
    notifyListeners();
  }
}
