import 'dart:convert';
import 'package:flutter/services.dart' show rootBundle;

//...
Future<Map<String, dynamic>> parseJsonFromAssets(String assetsPath) async {
  print('--- Parse json from: $assetsPath');
  return rootBundle
      .loadString(assetsPath)
      .then((jsonStr) => jsonDecode(jsonStr));
}

Future<String> _loadFromAsset() async {
  return await rootBundle.loadString("assets/data.json");
}

Future parseJson() async {
  String jsonString = await _loadFromAsset();
  final jsonResponse = jsonDecode(jsonString);
}
